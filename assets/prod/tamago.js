//Ma constante de nb random
const randomGen = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
}


// ------------------- Récup elem - déclaration var
// bars
let funBar = document.getElementById('funBar');
let teachBar = document.getElementById('teachBar');
let feedBar = document.getElementById('feedBar');
// btns
let funBtn = document.getElementById('funBtn');
let teachBtn = document.getElementById('teachBtn');
let feedBtn = document.getElementById('feedBtn');
// increase barLv.
let increaseFun = randomGen(20);
let increaseTeach = randomGen(17);
let increaseFeed = randomGen(23);







// ------------------- decrease funBar
let decreaseFunBar = window.setInterval(() => {
    let randomNb = randomGen(23); //23

    let dataValueStr = funBar.getAttribute("data-value"); // récupère la valeur de data-value
    let dataValueInt = parseInt(dataValueStr, 10); // convertit la string en int
    let newDataValue = dataValueInt - randomNb;

    if (newDataValue < 0) {
        newDataValue = 0;
        increaseFun = 0;
    }

    funBar.style.width = newDataValue + '%';
    funBar.setAttribute("data-value", newDataValue);
}, 1500);


// ----------------- decrease teachBar
let decreaseTeachBar = window.setInterval(() => {
    let randomNb = randomGen(10); // pour ne pas sortir de 0 : --> (max-min+1)
    // récupère la valeur de data-value :
    let dataValueStr = teachBar.getAttribute("data-value");
    // convertit la string en int :
    let dataValueInt = parseInt(dataValueStr, 10);
    // màj de la data-value
    let newDataValue = dataValueInt - randomNb;

    if (newDataValue < 0) {
        newDataValue = 0;
        increaseTeach = 0;
    }
    if (newDataValue == 0) {
        let imageCreature = document.getElementById('creature');
            imageCreature.setAttribute('src', './assets/img/5_graow.png');
    }


    teachBar.style.width = newDataValue + '%';
    teachBar.setAttribute("data-value", newDataValue);

    // DeadEnd
    if (newDataValue == 0) {
        let funBarLvl = funBar.getAttribute('data-value');
        let feedBarLvl = feedBar.getAttribute('data-value');
        let imageCreature = document.getElementById('creature');
        imageCreature.setAttribute('src', './assets/img/5_graow.png');

        if (funBarLvl == 0 && feedBarLvl == 0) {
            clearInterval(decreaseFunBar);
            clearInterval(decreaseTeachBar);
            clearInterval(decreaseFeedBar);

            let imageCreature = document.getElementById('creature');
            imageCreature.setAttribute('src', './assets/img/7_youreDead.png');
            let lose = document.getElementById('lose');
            lose.style.display = 'block';

            // window.setTimeout(() => {
            // }, 1);
        }
    }
}, 500);



// ----------------- decrease feedBar
let decreaseFeedBar = window.setInterval(() => {
    let randomNb = randomGen(15); //15

    let dataValueStr = feedBar.getAttribute("data-value");
    let dataValueInt = parseInt(dataValueStr, 10);
    let newDataValue = dataValueInt - randomNb;

    if (newDataValue < 0) {
        newDataValue = 0;
        increaseFeed = 0;
    }

    if (newDataValue == 0) {
        let imageCreature = document.getElementById('creature');
        imageCreature.setAttribute('src', './assets/img/5_graow.png');
    }

    // ajuste le niveau de la barre à la valeur de data-value :
    feedBar.style.width = newDataValue + '%';
    // met à jour la valeur de data-value dans le html :
    feedBar.setAttribute("data-value", newDataValue);

}, 1000);











// ------------------------- BTN
// v2. : augmentation différente en fonction de la hauteur de la barre ?

// funBtn

funBtn.addEventListener('click', () => {
    let dataValueStr = funBar.getAttribute('data-value');
    let dataValueInt = parseInt(dataValueStr, 10);
    let newDataValue = dataValueInt + increaseFun;

    if (newDataValue > 100) {
        newDataValue = 100;
    }

    if (newDataValue <= 0) { // Ça ne fonctionne pas mais j'y crois quand même. --'
        increaseFun = 0;
    }

    funBar.setAttribute('data-value', newDataValue);
    funBar.style.width = newDataValue + '%';
})

// teachBtn

teachBtn.addEventListener('click', () => {
    let dataValueStr = teachBar.getAttribute('data-value');
    let dataValueInt = parseInt(dataValueStr, 10);
    let newDataValue = dataValueInt + increaseTeach;

    if (newDataValue > 100) {
        newDataValue = 100;
    }

    if (newDataValue <= 0) {
    }

    teachBar.setAttribute('data-value', newDataValue);
    teachBar.style.width = newDataValue + '%';
})


// feedBtn

feedBtn.addEventListener('click', () => {
    let dataValueStr = feedBar.getAttribute('data-value');
    let dataValueInt = parseInt(dataValueStr, 10);
    let newDataValue = dataValueInt + increaseFeed;

    if (newDataValue > 100) {
        newDataValue = 100;
    }

    if (newDataValue <= 0) {
        increaseFeed = 0;
        newDataValue = 0;
    }

    feedBar.setAttribute('data-value', newDataValue);
    feedBar.style.width = newDataValue + '%';
})





// ------------------------- Console
// récupération de l'elem input
let input = document.getElementById('input')

input.addEventListener('keydown', function (event) {

    if (event.key === 'Enter') {
        // récupération de la valeur entrée par user dans input
        let userValue = input.value;

        switch (userValue) {
            case 'fun':
                let dataValueFunStr = funBar.getAttribute('data-value');
                let dataValueFunInt = parseInt(dataValueFunStr, 10);
                let newDataValueFun = dataValueFunInt + increaseFun;
                if (newDataValueFun > 100) {
                    newDataValueFun = 100;
                }
                if (newDataValueFun <= 0) {
                    increaseFun = 0;
                    newDataValueFun = 0;
                }
                funBar.setAttribute('data-value', newDataValueFun);
                funBar.style.width = newDataValueFun + '%';
                break;

            case 'teach':
                let dataValueTeachStr = teachBar.getAttribute('data-value');
                let dataValueTeachInt = parseInt(dataValueTeachStr, 10);
                let newDataValueTeach = dataValueTeachInt + increaseTeach;
                if (newDataValueTeach > 100) {
                    newDataValueTeach = 100;
                }
                if (newDataValueTeach <= 0) {
                    increaseTeach = 0;
                    newDataValueTeach = 0;
                }
                teachBar.setAttribute('data-value', newDataValueTeach);
                teachBar.style.width = newDataValueTeach + '%';
                break;

            case 'feed':
                let dataValueFeedStr = feedBar.getAttribute('data-value');
                let dataValueFeedInt = parseInt(dataValueFeedStr, 10);
                let newDataValueFeed = dataValueFeedInt + increaseFeed;

                if (newDataValueFeed > 100) {
                    newDataValueFeed = 100;
                }

                if (newDataValueFeed <= 0) {
                    increaseFeed = 0;
                    newDataValueFeed = 0;
                }

                feedBar.setAttribute('data-value', newDataValueFeed);
                feedBar.style.width = newDataValueFeed + '%';
                break;

            default:
                // récupération du placeholder de input, affaçage du message et indication en place holder
                let placeHolder = input.placeholder;
                input.value = '';
                input.setAttribute('placeHolder', "Erreur. Createurrr n'a pas besoin de cela, mais elle vous pardonne et vous prie de recommencer.");
        }
    }
})

// let history = document.getElementById('history');
// toDoooooooooooooooooOOOOOOOOOOOOOooooooooooOOOOOoooOOOoooOOOoooOOoooOoooOOoooOoooOoooOoooOOOOOOOOOOO


//VERSION RACCOURCIE
// window.setInterval(() => { // fc° fléchée de setInt
//     let randomNb = randomGen(4);
//     // test(resultFunBarLv);
//     funBar.setAttribute("data-value",
//         // parseInt = transfomer une string en int - base décimale ('10').
//         parseInt(funBar.getAttribute("data-value"), 10) - randomNb); 
//     funBar.style.width = `${funBar.getAttribute("data-value")}%`;
//     console.log(funBar.style.width);
//     return resultFunBarLv;
// }, 1000);


// -------------- DeadEnd

// --> "quand" ==> se fait sur la DURÉE (car qc qui se vérif dans le temps)
// --> Ne peut pas se faire de suite
// =  possiblement utilisation de l'asynchrone (ou des boucles)

// v2Mettre une phrase random pour le fun !!!


// --------------------- SÉPARER LE FONCTIONNEMENT DE L'AFFICHAGE
//--> Partie 1 : récupération des éléments ; 2: modification ; 3: maj de l'affichage.

// const test = (val) => {
//     funBar.setAttribute("data-value",
//         parseInt(funBar.getAttribute("data-value"), 10) - val);
//     funBar.style.width = `${funBar.getAttribute("data-value")}%`;
//     console.log(funBar.style.width);
// }

// let teachBarLv = () => window.setInterval (function() {
//     let resultTeachBarLv = randomGen();
//     return resultTeachBarLv;
// }, 5000)

// const feedBarLv = () => window.setInterval (function() {
//     let resultFeedBarLv = randomGen();
//     return resultFeedBarLv;
// }, 4000)


// ------------------- 3. màj affichage
// console.log(funBarLv());

// funBar.style.width = funBar.style.width  - funBarLv() +'%';
// console.log(funBar.style.width);
// teachBar.style.width = teachBarLv.style.width - teachteachBarLv + '%';
// feedBar.style.width = feedBarLv - feedBarLv + '%';




// window.setInterval(function() { // fonction anonyme : le couplage ne pose pas de pb car elle n'est pas reprise ailleurs (utilisée une seule fois ici).
//     let funWidth = funBar.style.width;
//     let val = random(3); 

//     While (funWidth < 0) {
//         return funWidth = funWidth - val + '%';
//     };

// }, 3000);
